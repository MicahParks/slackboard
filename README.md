# Slackboard
Do you check your grades too often? Become alpha nerd prime and have a program check your grades and email updates to you.

Pull requests or other contributions welcome.

(Currently only working for VCU students.)

## Setup
The recommended method would be to use docker. You will still need to set up a bot email.

**Making a bot email:**

* Slackboard assumes that you will be using a bot email hosted by Gmail. If this is not the case, overrride the SMTP server address with a command line argument. Figure out how to turn on SMTP for your bot email provider. 
* When using Gmail, you must enable using less secure apps. Google may slightly change the process of how to do this from time to time, so a good search term for this would be `Gmail turn on less secure apps`. This allows the use of SMTP, which is what Slackboard uses to send mail from your bot email.
* Do not use your bot email for anything else but bot stuff.

**Docker steps:**

1. Optional: Build the Docker image yourself with the `Dockerfile` in this project. (If skipped it will pull from my image on [micahparks/slackboard:vcu](https://cloud.docker.com/u/micahparks/repository/docker/micahparks/slackboard))
2. Make a directory called `slackboardvolume`.
3. Grab the `docker-compose.yml` file from this project and place it in the parent directory of `slackboardvolume`.
4. Edit the `docker-compose.yml` file to fit your circumstances. *The items you must change are denoted by* CAPITAL\_LETTERS\_WITH_UNDERSCORES.
5. Create the password files as specified in `docker-compose.yml` and put only the password characters in there. The email password is **for your bot email**.
6. Run `docker-compose up` in the directory containing your `docker-compose.yml` file.

**Ubuntu/Raspbian setup:**

1. Use the provided setup scripts. Scripts starting with `headlesswithoutdisplaysetup` will install the required packages to run a virtual display, because Selenium requires a display, even in headless mode.
2. Make the appropriate password files if desired and run with the correct command line arguments.

**NOTE:** The differences between the scripts ending with `.ubuntu.sh` and `.raspbian.sh` are the geckodriver and Firefox versions. The repositories for Raspbian do not contain an up to date version of Firefox (iceweasel must be used) and must use a compatible version of geckodriver. At the time of writing this, January 2019, the latest version of geckodriver that can be used on Raspbian is found at: [https://github.com/mozilla/geckodriver/releases/download/v0.15.0/geckodriver-v0.15.0-arm7hf.tar.gz](https://github.com/mozilla/geckodriver/releases/download/v0.15.0/geckodriver-v0.15.0-arm7hf.tar.gz).

**Background scripting:**

If you want Slackboard to run in the background of your server/Raspberry Pi or whatever, I reccomend writing a [cron job](https://www.ostechnix.com/a-beginners-guide-to-cron-jobs/https://kb.iu.edu/d/afiz) the executes the program every two hours or so.

## How does it work?

Using [Selenium](https://docs.seleniumhq.org/), python will log into [https://blackboard.vcu.edu](https://blackboard.vcu.edu) for you, navigate to your grades, and grab all of the elements relating to your graded assignments for all of your classes. Using these elements, it will determine what is new/updated information based on the previous session (if there was a previous session) and it will email you the difference. It will then save the new data to the disk for the future.

## Noding
If you take a look at another one of my projects called [noding](https://gitlab.com/MicahParks/noding), which is very poorly documented at the moment, you can get this working on a server node and make your Android phone a client node. You can then configure a [Termux](https://termux.com/) shortcut so you can press a button and getcha grades like a nerd.

*If there is interest in other people doing this. I can write up some better documentation for it.*

## To Do
- [ ] Use environment variables for passwords in addition to the option of files. (Updates for python files and docker-compose.yml)
- [ ] Use a logging config instead of just `import logging`.