#!/usr/bin/env bash
sudo apt update
sudo apt -f install -y
sudo apt full-upgrade -y
sudo apt autoremove -y
sudo apt autoclean -y
sudo apt install iceweasel python3-pip xvfb -y
sudo pip3 install pyvirtualdisplay
sudo pip3 install selenium
wget https://github.com/mozilla/geckodriver/releases/download/v0.15.0/geckodriver-v0.15.0-arm7hf.tar.gz -O geckodriver.tar.gz
tar xzf geckodriver.tar.gz
rm geckodriver.tar.gz
sudo mv geckodriver /usr/bin/geckodriver
