#!/usr/bin/env python
"""
"""

# Start standard library injections.
from argparse import ArgumentParser, FileType
from pickle import load
# End standard library injections.

# Start third party injections.
# End third party injections.

# Start custom injections.
# End custom injections.


def main(pickleFile):
    """
    """
    classObjList = load(pickleFile)
    pickleFile.close()
    for nowClassObj in classObjList:
        print(nowClassObj)


if __name__ == '__main__':
    ARG_PARSER = ArgumentParser(description='MISSING')
    ARG_PARSER.add_argument('pickleFile', help='The path to the pickle file created by Slackboard.',
                            type=FileType('rb'))
    ARG_NAMESPACE = ARG_PARSER.parse_args()
    main(pickleFile=ARG_NAMESPACE.pickleFile)
