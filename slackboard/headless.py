#!/usr/bin/env python3
"""
"""

# Start standard library injections.
from argparse import ArgumentParser, ArgumentTypeError, FileType, Namespace
from getpass import getpass
import logging
from pickle import dump, load
from smtplib import SMTP
from typing import List, Set, TextIO, Tuple
# End standard library injections.

# Start third party injections.
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
# End third party injections.

# Start custom injections.
from slackboard.objects.assignmentobj import Assignment
from slackboard.objects.classobj import Class, old_vs_new_class_diff
# End custom injections.


BLACKBOARD_ADDRESS_STR = 'https://blackboard.vcu.edu/'
DEFAULT_PORT_INT = 587
DEFAULT_SMTP_SERVER_ADDRESS_STR = 'smtp.gmail.com'
ENTER_KEY_STR = '\ue007'
MAX_ASSIGNMENTS_INT = 100
MAX_CLASSES_INT = 10
MAX_WAIT_SECONDS_INT = 30
DEFAULT_PICKLE_FILE_PATH_STR = 'slackboard.pickle'
WEB_DRIVER_WAIT_OBJ = None


def email(bodyStr: str, toEmailStr: str, passwordStr: str, fromEmailStr: str, subjectStr: str,
          portInt: int = DEFAULT_PORT_INT, smtpServerAddressStr: str = DEFAULT_SMTP_SERVER_ADDRESS_STR) -> None:
    """
    https://stackoverflow.com/questions/10147455/how-to-send-an-email-with-gmail-as-provider-using-python

    Check this link for issues with logging in.
    """
    try:
        smtpObj = SMTP(host=smtpServerAddressStr, port=portInt)
        smtpObj.ehlo()
        smtpObj.starttls()
        smtpObj.ehlo()
    except Exception as exceptionStr:
        logging.critical(msg='Failed to ehlo or start TLS with server "{}". Error: {}'.format(smtpServerAddressStr,
                                                                                              exceptionStr))
        print('Failed to ehlo or start TLS with server "{}". Error: {}'.format(smtpServerAddressStr, exceptionStr))
        quit(5)
    smtpObj.login(user=fromEmailStr, password=passwordStr)
    emailContentStr = 'From: {}\nTo: {}\nSubject: {}\n\n{}'.format(fromEmailStr, toEmailStr, subjectStr, bodyStr)
    logging.info(msg='Sending Slackboard email update to "{}".'.format(toEmailStr))
    smtpObj.sendmail(fromEmailStr, toEmailStr, emailContentStr)
    smtpObj.close()


def get_class_obj_list(firefoxWebdriver: WebDriver) -> List[Class]:
    """
    """
    dropDownMenuXpathStr = '//*[@id="global-nav-link"]'
    web_driver_wait(firefoxWebdriver, dropDownMenuXpathStr)
    dropDownMenu = firefoxWebdriver.find_element_by_xpath(dropDownMenuXpathStr)
    dropDownMenu.click()
    gradesIconXpathStr = '//*[@id="MyGradesOnMyBb_____MyGradesTool"]'
    web_driver_wait(firefoxWebdriver, gradesIconXpathStr)
    gradesIcon = firefoxWebdriver.find_element_by_xpath(gradesIconXpathStr)
    gradesIcon.click()
    iFrameXpathStr = '//*[@id="mybbCanvas"]'
    web_driver_wait(firefoxWebdriver, iFrameXpathStr)
    firefoxWebdriver.switch_to.frame('mybbCanvas')
    classObjList = list()
    foundClassBool = False
    for nowClassTabIndexInt in range(MAX_CLASSES_INT):
        nowClassTabIndexInt += 1
        classNameXpathStr = '/html/body/div/div/div/div/div/div[2]/div[2]/div[2]/div[{}]/div[4]/span'.format(
            nowClassTabIndexInt)
        if foundClassBool is False:
            web_driver_wait(firefoxWebdriver, classNameXpathStr)
        try:
            className = firefoxWebdriver.find_element_by_xpath(classNameXpathStr)
            foundClassBool = True
        except NoSuchElementException:
            if foundClassBool is True:
                break
            raise NotImplementedError("This shouldn't happen...")
        classNameStr = className.text
        classObj = Class(nameStr=classNameStr)
        className.click()
        classIFrameXpathStr = '//*[@id="right_stream_mygrades"]'
        web_driver_wait(firefoxWebdriver, classIFrameXpathStr)
        firefoxWebdriver.switch_to.frame('right_stream_mygrades')
        web_driver_wait(firefoxWebdriver, '/html/body/div/div/div[2]/div[1]/div/div[2]/div[2]/div[1]/div[3]')
        for nowAssignmentEntryIndexInt in range(MAX_ASSIGNMENTS_INT):
            nowAssignmentEntryIndexInt += 1
            try:
                maxScoreXpathStr = '/html/body/div/div/div[2]/div[1]/div/div[2]/div[2]/div[{}]/div[3]/span[2]'.format(
                    nowAssignmentEntryIndexInt)
                nameXpathStr = '/html/body/div/div/div[2]/div[1]/div/div[2]/div[2]/div[{}]/div[1]/a'.format(
                    nowAssignmentEntryIndexInt)
                scoreXpathStr = '/html/body/div/div/div[2]/div[1]/div/div[2]/div[2]/div[{}]/div[3]/span[1]'.format(
                    nowAssignmentEntryIndexInt)
                scoreFloatStrElem = firefoxWebdriver.find_element_by_xpath(scoreXpathStr)
                firefoxWebdriver.execute_script("return arguments[0].scrollIntoView();", scoreFloatStrElem)
                scoreFloatStr = scoreFloatStrElem.text
                maxScoreFloatStrElem = firefoxWebdriver.find_element_by_xpath(maxScoreXpathStr)
                firefoxWebdriver.execute_script("return arguments[0].scrollIntoView();", maxScoreFloatStrElem)
                maxScoreFloatStr = maxScoreFloatStrElem.text.lstrip('/')
                if maxScoreFloatStr == str():
                    maxScoreFloatStr = 1  # Extra credit without defined max score.
                try:
                    nameStrElem = firefoxWebdriver.find_element_by_xpath(nameXpathStr)
                    firefoxWebdriver.execute_script("return arguments[0].scrollIntoView();", nameStrElem)
                    nameStr = nameStrElem.text
                except NoSuchElementException:
                    nameStr = firefoxWebdriver.find_element_by_xpath(nameXpathStr.rstrip('/a')).text.split('\n')[0]
                if scoreFloatStr in [str(), '-'] or '%' in scoreFloatStr:
                    continue
                classObj.assignmentObjList.append(Assignment(maxScoreFloat=float(maxScoreFloatStr), nameStr=nameStr,
                                                             scoreFloat=float(scoreFloatStr)))
            except NoSuchElementException:
                pass
        firefoxWebdriver.switch_to.parent_frame()
        classObjList.append(classObj)
    firefoxWebdriver.switch_to.parent_frame()
    return classObjList


def get_password_str_from_file(inFile: TextIO) -> str:
    """
    """
    logging.info(msg='Attempting to read password from "{}".'.format(inFile.name))
    inFile.seek(0)
    return inFile.read().strip()


def is_valid_port_int(portIntStr: str) -> int:
    """
    Help argparse determine if the port number is valid.
    """
    try:
        portInt = int(portIntStr)
    except ValueError:
        raise ArgumentTypeError('Cannot convert "{}" to an integer.'.format(portIntStr))
    if 1024 < portInt <= 65535:
        return portInt
    raise ArgumentTypeError('Port not between > 1024 and <= 65535.')


def load_slackboard_pickle_file(filePathStr: str = DEFAULT_PICKLE_FILE_PATH_STR) -> List[Class]:
    """
    """
    try:
        with open(filePathStr, 'rb') as inFile:
            classObjList = load(inFile)
        for nowClassObj in classObjList:
            if not isinstance(nowClassObj, Class):
                return None
    except Exception as exceptionStr:
        logging.warning(msg='Could not load pickle file: "{}". Exception: {}'.format(filePathStr, exceptionStr))
        return None
    return classObjList


def login_to_blackboard(passwordStr: str, usernameStr: str) -> WebDriver:
    """
    """
    options = Options()
    options.headless = True
    firefoxWebdriver = webdriver.Firefox(options=options)
    firefoxWebdriver.get(BLACKBOARD_ADDRESS_STR)
    eLearningLoginButtonXpathStr = '/html/body/div[5]/div/div/div/div/div/div/div/div[2]/div[1]/div[2]/div/div/div/div[2]/div/p/a/img'
    web_driver_wait(firefoxWebdriver, eLearningLoginButtonXpathStr)
    eLearningLoginButton = firefoxWebdriver.find_element_by_xpath(eLearningLoginButtonXpathStr)
    eLearningLoginButton.click()
    vcuEidXpathStr = '//*[@id="username"]'
    web_driver_wait(firefoxWebdriver, vcuEidXpathStr)
    vcuEidInput = firefoxWebdriver.find_element_by_xpath(vcuEidXpathStr)
    vcuEidInput.send_keys(usernameStr)
    passwordXpathStr = '//*[@id="password"]'
    passwordInput = firefoxWebdriver.find_element_by_xpath(passwordXpathStr)
    passwordInput.send_keys(passwordStr + ENTER_KEY_STR)
    return firefoxWebdriver


def main(argNamespace: Namespace) -> None:
    """
    """
    blackboardPasswordFile = argNamespace.blackboardPasswordFile
    if blackboardPasswordFile is None:
        blackboardPasswordStr = getpass('Blackboard password: ')
    else:
        blackboardPasswordStr = get_password_str_from_file(inFile=argNamespace.blackboardPasswordFile)
    emailPasswordFile = argNamespace.emailPasswordFile
    if emailPasswordFile is None:
        emailPasswordStr = getpass('Email password: ')
    else:
        emailPasswordStr = get_password_str_from_file(inFile=argNamespace.emailPasswordFile)
    fromEmailStr = argNamespace.fromEmailStr
    pickleFilePathStr = argNamespace.pickleFilePathStr
    portInt = argNamespace.portInt
    smtpServerAddressStr = argNamespace.smtpServerAddressStr
    toEmailStr = argNamespace.toEmailStr
    usernameStr = argNamespace.usernameStr
    oldClassObjList = load_slackboard_pickle_file(filePathStr=pickleFilePathStr)
    firefoxWebdriver = login_to_blackboard(passwordStr=blackboardPasswordStr, usernameStr=usernameStr)
    try:
        blackboardPasswordFile.close()
    except AttributeError:
        pass
    newClassObjList = get_class_obj_list(firefoxWebdriver=firefoxWebdriver)
    quit_web_driver(firefoxWebdriver=firefoxWebdriver)
    bodyStr, subjectStr = make_body_str_and_subject_str(newClassObjList=newClassObjList,
                                                        oldClassObjList=oldClassObjList)
    bodyStr = bodyStr.strip()
    subjectStr = subjectStr.strip()
    if len(bodyStr) == 0 or len(subjectStr) == 0:
        logging.info(msg='No change. Quitting.')
        quit()
    email(bodyStr=bodyStr, fromEmailStr=fromEmailStr, passwordStr=emailPasswordStr, portInt=portInt,
          smtpServerAddressStr=smtpServerAddressStr, subjectStr=subjectStr, toEmailStr=toEmailStr)
    try:
        emailPasswordFile.close()
    except AttributeError:
        pass
    logging.info(msg='Writing most recent data to disk at: "{}".'.format(pickleFilePathStr))
    with open(pickleFilePathStr, 'wb') as outFile:
        dump(newClassObjList, outFile)


def make_class_diff_str(classNameStr: str, newAssigmentSet: Set[Assignment],
                        updatedAssignmentTupleSet: Set[Tuple[Assignment, Assignment]]) -> str:
    """
    """
    newAssigmentStr = str()
    updatedAssignmentStr = str()
    printedClassNameStrBool = False
    if len(newAssigmentSet) != 0:
        newAssigmentStr = classNameStr + '\n'
        printedClassNameStrBool = True
        newAssigmentStr += 'New grades have been entered:\n'
        for nowAssignmentTuple in newAssigmentSet:
            percentageFloat = nowAssignmentTuple.scoreFloat / nowAssignmentTuple.maxScoreFloat * 100
            newAssigmentStr += '  {}/{} --> {}%\n'.format(nowAssignmentTuple.scoreFloat,
                                                          nowAssignmentTuple.maxScoreFloat,
                                                          round(percentageFloat, 2))
        newAssigmentStr = newAssigmentStr.strip()
    if len(updatedAssignmentTupleSet) != 0:
        updatedAssignmentStr = str()
        if printedClassNameStrBool is False:
            updatedAssignmentStr = classNameStr + '\n'
        updatedAssignmentStr += 'These assignments have been updated:\n'
        for nowAssignmentTuple in updatedAssignmentTupleSet:
            percentageFloat = nowAssignmentTuple[0].scoreFloat / nowAssignmentTuple[0].maxScoreFloat * 100
            updatedAssignmentStr += '  Old: {}: {}/{} --> {}%\n'.format(nowAssignmentTuple[0].nameStr,
                                                                        nowAssignmentTuple[0].scoreFloat,
                                                                        nowAssignmentTuple[0].maxScoreFloat,
                                                                        round(percentageFloat, 2))
            percentageFloat = nowAssignmentTuple[1].scoreFloat / nowAssignmentTuple[1].maxScoreFloat * 100
            updatedAssignmentStr += '  Updated: {}: {}/{} --> {}%\n'.format(nowAssignmentTuple[1].nameStr,
                                                                            nowAssignmentTuple[1].scoreFloat,
                                                                            nowAssignmentTuple[1].maxScoreFloat,
                                                                            round(percentageFloat, 2))
    if len(newAssigmentStr) != 0 and len(updatedAssignmentStr) != 0:
        updatedAssignmentStr = '\n' + updatedAssignmentStr
    return newAssigmentStr + updatedAssignmentStr


def make_body_str_and_subject_str(newClassObjList: List[Class], oldClassObjList: List[Class]) -> Tuple[str, str]:
    """
    """
    bodyStr = str()
    subjectStr = str()
    newAssigmentBool = False
    newClassesStr = str()
    updatedAssignmentTupleBool = False
    if oldClassObjList is None:
        for nowNewClassObj in newClassObjList:
            bodyStr += str(nowNewClassObj)
        subjectStr = 'Slackboard update'
    else:
        for nowNewClassObj in newClassObjList:
            classNameStr = nowNewClassObj.nameStr
            foundClassNameStrBool = False
            for nowOldClassObj in oldClassObjList:
                if nowOldClassObj.nameStr == classNameStr:
                    newAssigmentSet, updatedAssignmentTupleSet = old_vs_new_class_diff(oldClassObj=nowOldClassObj,
                                                                                       newClassObj=nowNewClassObj)
                    if len(newAssigmentSet) > 0:
                        newAssigmentBool = True
                    if len(updatedAssignmentTupleSet) > 0:
                        updatedAssignmentTupleBool = True
                    foundClassNameStrBool = True
                    if len(newAssigmentSet) > 0 or len(updatedAssignmentTupleSet) > 0:
                        bodyStr += make_class_diff_str(classNameStr=classNameStr, newAssigmentSet=newAssigmentSet,
                                                       updatedAssignmentTupleSet=updatedAssignmentTupleSet)
                    break
            if foundClassNameStrBool is False:
                if len(bodyStr) == 0:
                    newClassesStr += 'This class was just added:\n'
                newClassesStr += str(nowNewClassObj)
    bodyStr += '\n' + newClassesStr
    if newAssigmentBool is True:
        subjectStr = 'Slackboard: New assignments have been posted.'
    if updatedAssignmentTupleBool is True:
        subjectStr = 'Slackboard: Updated assignments have been posted.'
    if newAssigmentBool is True and updatedAssignmentTupleBool is True:
        subjectStr = 'Slackboard: New and updated assignments have been posted.'
    return bodyStr, subjectStr


def noding(fromEmailStr: str, portInt: int, toEmailStr: str, usernameStr: str, emailBool: bool = True,
           pickleFilePathStr: str = DEFAULT_PICKLE_FILE_PATH_STR,
           smtpServerAddressStr: str = DEFAULT_SMTP_SERVER_ADDRESS_STR):
    """
    """
    with open('blackboardpassword.txt') as inFile:
        blackboardPasswordStr = get_password_str_from_file(inFile=inFile)
    with open('emailpassword.txt') as inFile:
        emailPasswordStr = get_password_str_from_file(inFile=inFile)
    oldClassObjList = load_slackboard_pickle_file(filePathStr=pickleFilePathStr)
    firefoxWebdriver = login_to_blackboard(passwordStr=blackboardPasswordStr, usernameStr=usernameStr)
    newClassObjList = get_class_obj_list(firefoxWebdriver=firefoxWebdriver)
    quit_web_driver(firefoxWebdriver=firefoxWebdriver)
    bodyStr, subjectStr = make_body_str_and_subject_str(newClassObjList=newClassObjList,
                                                        oldClassObjList=oldClassObjList)
    bodyStr = bodyStr.strip()
    subjectStr = subjectStr.strip()
    if len(bodyStr) == 0 or len(subjectStr) == 0:
        logging.info(msg='No change. Quitting.')
        return 'No change.'
    if emailBool is True:
        email(bodyStr=bodyStr, fromEmailStr=fromEmailStr, passwordStr=emailPasswordStr, portInt=portInt,
              smtpServerAddressStr=smtpServerAddressStr, subjectStr=subjectStr, toEmailStr=toEmailStr)
    logging.info(msg='Writing most recent data to disk at: "{}".'.format(pickleFilePathStr))
    with open(pickleFilePathStr, 'wb') as outFile:
        dump(newClassObjList, outFile)
    return bodyStr


def web_driver_wait(firefoxWebdriver: WebDriver, xpathStr: str, maxWaitSecondsInt: int = MAX_WAIT_SECONDS_INT) -> None:
    """
    """
    global WEB_DRIVER_WAIT_OBJ
    if WEB_DRIVER_WAIT_OBJ is None:
        WEB_DRIVER_WAIT_OBJ = WebDriverWait(firefoxWebdriver, maxWaitSecondsInt)
    WEB_DRIVER_WAIT_OBJ.until(expected_conditions.presence_of_element_located((By.XPATH, xpathStr)))


def quit_web_driver(firefoxWebdriver: WebDriver) -> None:
    """
    """
    logoutButtonXpathStr = '//*[@id="topframe.logout.label"]'
    logoutButton = firefoxWebdriver.find_element_by_xpath(logoutButtonXpathStr)
    logoutButton.click()
    firefoxWebdriver.quit()


if __name__ == '__main__':
    ARG_PARSER = ArgumentParser(description='MISSING')
    ARG_PARSER.add_argument('-a', '--smtp-server-address', default=DEFAULT_SMTP_SERVER_ADDRESS_STR,
                            dest='smtpServerAddressStr',
                            help='The address of the SMTP server to log into to send updates through.', type=str)
    ARG_PARSER.add_argument('--email-password-file', default=None, dest='emailPasswordFile',
                            help='The path to the file that contains the password to log into the sending email. If no '
                                 'file is provided, it will be prompted at runtime.', type=FileType('r'))
    ARG_PARSER.add_argument('-f', '--from-email', dest='fromEmailStr',
                            help='Email address to send Slackboard updated from.', required=True, type=str)
    ARG_PARSER.add_argument('-l', '--log-file', default='slackboard.log', dest='logFilePathStr',
                            help='The path of the file where the logs for this program should be stored.', type=str)
    ARG_PARSER.add_argument('-p', '--blackboard-password-file', default=None, dest='blackboardPasswordFile',
                            help='The path to the file that contains the password to log into Blackboard. If no file '
                                 'is provided, it will be prompted at runtime.',
                            type=FileType('r'))
    ARG_PARSER.add_argument('--pickle', '--pickle-file', default=DEFAULT_PICKLE_FILE_PATH_STR, dest='pickleFilePathStr',
                            help="The path to the file that the previous session's data resides and this session's data"
                                 " should be saved to.", type=str)
    ARG_PARSER.add_argument('--port', '--port-int', default=DEFAULT_PORT_INT, dest='portInt',
                            help='The port to use when logging into the SMTP server.', type=is_valid_port_int)
    ARG_PARSER.add_argument('-t', '--to-email', dest='toEmailStr', help='Email address to send Slackboard updates to.',
                            required=True, type=str)
    ARG_PARSER.add_argument('-u', '--blackboard-username', dest='usernameStr',
                            help='The username to log into Blackboard with.', required=True, type=str)
    ARG_NAMESPACE = ARG_PARSER.parse_args()
    logging.basicConfig(datefmt='%Y%m%d %H:%M:%S', filename=ARG_NAMESPACE.logFilePathStr,
                        format='%(levelname)s| |%(asctime)s| |%(message)s| |', level=logging.INFO)
    logging.info(msg='Entering "main".')
    main(argNamespace=ARG_NAMESPACE)
