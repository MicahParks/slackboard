"""
"""
# Start standard library injections.
from typing import Set, Tuple
# End standard library injections.

# Start custom injections.
from slackboard.objects.assignmentobj import Assignment
# End custom injections.


class Class:
    def __init__(self, nameStr: str):
        self.nameStr = nameStr
        self.assignmentObjList = list()

    def __str__(self) -> str:
        returnStr = '{}:\n'.format(self.nameStr)
        for nowAssignmentObj in self.assignmentObjList:
            percentageFloat = nowAssignmentObj.scoreFloat / nowAssignmentObj.maxScoreFloat * 100
            returnStr += '  {}: {}/{} --> {}%\n'.format(nowAssignmentObj.nameStr, nowAssignmentObj.scoreFloat,
                                                        nowAssignmentObj.maxScoreFloat, round(percentageFloat, 2))
        return returnStr


def old_vs_new_class_diff(oldClassObj: Class, newClassObj: Class) -> Tuple[
    Set[Assignment], Set[Tuple[Assignment, Assignment]]]:
    """
    """
    newAssigmentSet = set()
    updatedAssignmentTupleSet = set()
    for newAssignmentObj in newClassObj.assignmentObjList:
        if newAssignmentObj not in newClassObj.assignmentObjList:
            newAssigmentSet.add(newAssignmentObj)
            continue
        for oldAssignmentObj in oldClassObj.assignmentObjList:
            if newAssignmentObj.nameStr == oldAssignmentObj.nameStr:
                if newAssignmentObj.scoreFloat != oldAssignmentObj.scoreFloat or \
                        newAssignmentObj.maxScoreFloat != oldAssignmentObj.maxScoreFloat:
                    updatedAssignmentTupleSet.add((oldAssignmentObj, newAssignmentObj))
    return newAssigmentSet, updatedAssignmentTupleSet
