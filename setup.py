"""
"""

# Start standard library injections.
from setuptools import setup
# End standard library injections.

if __name__ == '__main__':
    setup(name='slackboard', packages=['slackboard', 'slackboard/objects'], version='developer')
