#!/usr/bin/env bash
sudo apt update
sudo apt -f install -y
sudo apt full-upgrade -y
sudo apt autoremove -y
sudo apt autoclean -y
sudo apt install firefox python3-pip -y
sudo pip3 install selenium
wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-linux64.tar.gz -O geckodriver.tar.gz  # Update this address as newer versions ar released.
tar xzf geckodriver.tar.gz
rm geckodriver.tar.gz
sudo mv geckodriver /usr/bin/geckodriver
